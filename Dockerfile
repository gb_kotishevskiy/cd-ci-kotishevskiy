# Используем Alpine Linux в качестве базового образа
FROM alpine:latest

# Обновляем индекс пакетов и устанавливаем jq
RUN apk update && apk add jq

# Вывод версии jq для подтверждения успешной установки
RUN jq --version || echo "jq is not installed"

# Создание файла house.txt и добавление в него строк
RUN echo "one" >> house.txt &&\
    echo "two" >> house.txt
